- 👋 Hi, I’m Arthur!
- 📚 Studying engineering at EFREI Paris
- 👀 I’m interested in data analysis and finance
- 🌱 I’m currently learning Python and C
- 📫 You can reach me directly on github!

<!---
arthur-gtgn/arthur-gtgn is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
